# Francesco's arc42 mkdocs-material template

Want to create a documentation using the [arc42 template](https://arc42.org/) and [mkdocs-material](https://squidfunk.github.io/mkdocs-material/) starting from my template?
Be sure to have installed [cookiecutter](https://cookiecutter.readthedocs.io) and run:
```shell
cookiecutter https://gitlab.com/francesco-calcavecchia/arc42-mkdocs-material-template
```
